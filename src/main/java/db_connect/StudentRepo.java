package db_connect;

import model.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        // Need to be implemented
        // String query = String.format("INSERT INTO student(name, age, gender, phone) VALUES('%s',%d,'%s','%s')",
        //        student.getName(), student.getAge(), student.getGender(), student.getPhone());
        String query = "INSERT INTO student(name, age, gender, phone) VALUES(?, ?, ?, ?)";

        try {
            PreparedStatement preparedStmt = dbConnector.createConnect().prepareStatement(query);
            preparedStmt.execute("USE student_manage_sys_fanyue; ");
            preparedStmt.setString (1, student.getName());
            preparedStmt.setInt (2, student.getAge());
            preparedStmt.setString (3, student.getGender());
            preparedStmt.setString (4, student.getPhone());
            return preparedStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        }
        return false;
    }
    
    public List<Student> findStudents() {
        // Need to be implemented
        List<Student> list = new ArrayList<>();
        String query = "SELECT * from student";
        try (Statement statement = dbConnector.createConnect().createStatement()) {
            statement.execute("USE student_manage_sys_fanyue;");
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                String gender = resultSet.getString("gender");
                String phone = resultSet.getString("phone");

                Student student = new Student(id, name, age, gender, phone);
                list.add(student);
            }
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        }

        return list;
    }
    
    public boolean updateStudent(Student student) {
        // Need to be implemented
        String query = "UPDATE student SET name = ?, age = ?, gender = ?, phone = ? WHERE id = ?";

        try {
            PreparedStatement preparedStmt = dbConnector.createConnect().prepareStatement(query);
            preparedStmt.execute("USE student_manage_sys_fanyue; ");
            preparedStmt.setString (1, student.getName());
            preparedStmt.setInt (2, student.getAge());
            preparedStmt.setString (3, student.getGender());
            preparedStmt.setString (4, student.getPhone());
            preparedStmt.setInt(5, student.getId());

            return preparedStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        }

        return false;
    }
    
    public boolean deleteStudent(Student student) {
        // Need to be implemented
        String query = "DELETE FROM student WHERE id = ?";

        try {
            PreparedStatement preparedStmt = dbConnector.createConnect().prepareStatement(query);
            preparedStmt.execute("USE student_manage_sys_fanyue; ");

            preparedStmt.setInt (1, student.getId());
            return preparedStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        }

        return false;
    }
}
